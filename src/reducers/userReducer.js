function reducer (state = {}, action) {
  switch (action.type) {
    case 'LOGOUT':
      return Object.assign({}, state, { token: null })
    case 'LOGIN':
      return Object.assign({}, state, action.payload)
    default:
      return state
  }
}

export default reducer
