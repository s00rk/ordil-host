const URL_PATH = 'ec2-52-32-206-212.us-west-2.compute.amazonaws.com/'
//let URL_PATH = window.location.origin.replace('http://', '').replace('https://', '') + '/'

export default {
  URL: 'http://' + URL_PATH + 'api/v1/',
  URL_LOGIN: 'http://' + URL_PATH + 'api-auth/',
  URL_WS: 'ws://' + URL_PATH
}
