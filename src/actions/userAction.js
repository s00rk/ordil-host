import axios from 'axios'
import constants from '../constants.js'

export const login = (user) => {
  return (dispatch) => dispatch({ type: 'LOGIN', payload: user })
}

export const logout = () => {
  window.ws.close()
  return (dispatch) => dispatch({ type: 'LOGOUT' })
}

export const getUser = () => {
  return (dispatch) => {
    axios.get(constants.URL + 'hosts')
    .then((response) => {
      if (response.data.objects.length !== 1) {
        this.setState({ errorLogin: 'User not found', isLogin: false })
      } else {
        let user = response.data.objects[0]
        user['token'] = axios.defaults.headers.common['Authorization']
        dispatch({ type: 'LOGIN', payload: user })
      }
    })
    .catch((err) => {
      dispatch({ type: 'LOGOUT' })
      console.log(err)
    })
  }
}
