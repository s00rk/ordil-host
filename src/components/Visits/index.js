import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import _ from 'lodash'
import moment from 'moment'

import { Card } from 'material-ui/Card'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import FontIcon from 'material-ui/FontIcon'
import RaisedButton from 'material-ui/RaisedButton'
import Avatar from 'material-ui/Avatar'
import DatePicker from 'material-ui/DatePicker'
import TextField from 'material-ui/TextField'

import Visit from '../Visit'

import constants from '../../constants.js'
import './visit.css'

const root = {
  flexWrap: 'wrap',
  justifyContent: 'center',
  margin: 'auto',
  alignItems: 'center',
  display: 'flex'
}

const card = {
  width: '80%',
  marginTop: '4em'
}

class Visits extends Component {
  componentDidMount () {
    setInterval(() => this.setState({ time: Date.now() }), 1000)
  }

  constructor (props) {
    super(props)

    this.state = {
      data: [],
      page: 1,
      max: 20,
      visit: null,
      oldData: null
    }

    axios.defaults.headers.common['Authorization'] = 'Token ' + this.props.user.token
    axios.post(constants.URL + 'hosts/visits', {})
    .then((response) => {
      let data = _.filter(response.data, (visit) => visit.visitor_email !== this.props.user.email)
      this.setState({ data: data })
    })
    .catch((error) => {
      console.log(error)
    })

    this.changeStatus = this.changeStatus.bind(this)
    this.nextPage = this.nextPage.bind(this)
    this.backPage = this.backPage.bind(this)
    this.changeDate = this.changeDate.bind(this)
    this.onChangeStatus = this.onChangeStatus.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.convertToBin = this.convertToBin.bind(this)

    window.ws = new window.WebSocket(constants.URL_WS + this.convertToBin(this.props.user.email))
    window.ws.onmessage = (e) => {
      console.log(e)
      let newVisit = JSON.parse(e.data)
      let exists = false
      let data = _.map(this.state.data, (visit) => {
        if (visit.id === newVisit.id) {
          exists = true
          return newVisit
        } else {
          return visit
        }
      })

      if (!exists) {
        data.push(newVisit)
        if ('Notification' in window) {
          window.Notification.requestPermission().then((result) => {
            if (result === 'granted') {
              let options = {
                body: 'Tienes una nueva visita',
                icon: window.location.origin + '/static/ordillogo.png'
              }
              let notification = new window.Notification('Ordil ID', options)
              if ('Audio' in window) {
                let audio = new window.Audio(window.location.origin + '/static/audio/notification.mp3')
                audio.play()
              }
              setTimeout(notification.close.bind(notification), 5000)
            }
          })
        }
      }

      let oldData = []
      if (this.state.oldData !== null) {
        if (!exists) {
          oldData = this.state.oldData
          oldData.push(newVisit)
        } else {
          oldData = _.map(this.state.oldData, (visit) => {
            if (visit.id === newVisit.id) {
              return newVisit
            } else {
              return visit
            }
          })
        }
      }
      this.setState({ data: data, oldData: oldData })
    }
  }

  convertToBin (email) {
    let output = _.replace(email, /\./g, '')
    output = _.replace(output, new RegExp('@', 'g'), '')
    return output
  }

  changeStatus (rowNumber, columnId) {
    let data = _.chunk(this.state.data, this.state.max)
    data = data[ this.state.page - 1 ]
    let visit = data[rowNumber]
    this.setState({ visit: visit })
  }
  onChangeStatus (visit) {
    let data = _.map(this.state.data, (oldvisit) => {
      if (oldvisit.id === visit.id) {
        return visit
      } else {
        return oldvisit
      }
    })
    this.setState({ visit: null, data: data })
  }

  handleSearch (e, name) {
    name = name.toLowerCase()
    if (name.trim().length === 0) {
      this.setState({ data: this.state.oldData, oldData: null })
      return
    }
    let oldData = this.state.data
    if (this.state.oldData !== null) {
      oldData = this.state.oldData
    }

    let data = _.filter(oldData, (visit) => {
      let fullName = visit.visitor_first_name.toLowerCase() + ' ' + visit.visitor_last_name.toLowerCase()
      return visit.visitor_first_name.toLowerCase().indexOf(name) >= 0 || visit.visitor_last_name.toLowerCase().indexOf(name) >= 0 || fullName.indexOf(name) >= 0
    })
    oldData = this.state.data
    if (this.state.oldData !== null) {
      oldData = this.state.oldData
    }
    this.setState({ data: data, oldData: oldData })
  }

  nextPage () {
    let page = this.state.page + 1
    this.setState({ page: page })
  }

  backPage () {
    let page = this.state.page - 1
    this.setState({ page: page })
  }

  changeDate (e, date) {
    axios.post(constants.URL + 'hosts/visits', { date: moment(date).format('YYYY-MM-DD') })
    .then((response) => {
      let data = _.filter(response.data, (visit) => visit.visitor_email !== this.props.user.email)
      this.setState({ data: data })
    })
    .catch((error) => {
      console.log(error)
    })
  }

  render () {
    let data = _.chunk(this.state.data, this.state.max)
    let pages = data.length
    let page = this.state.page - 1
    data = data[page]
    if (data === undefined) {
      data = []
    }

    let nextBtn = null
    let backBtn = null

    if (pages >= 1) {
      if (this.state.page !== pages) {
        nextBtn = <RaisedButton onClick={this.nextPage} style={{float: 'right'}} icon={<i className='fa fa-arrow-right' aria-hidden='true' />} />
      } else {
        nextBtn = <RaisedButton disabled style={{float: 'right'}} icon={<i className='fa fa-arrow-right' aria-hidden='true' />} />
      }
      if (this.state.page !== 1) {
        backBtn = <RaisedButton onClick={this.backPage} style={{float: 'right', marginRight: '1em'}} icon={<i className='fa fa-arrow-left' aria-hidden='true' />} />
      } else {
        backBtn = <RaisedButton disabled style={{float: 'right', marginRight: '1em'}} icon={<i className='fa fa-arrow-left' aria-hidden='true' />} />
      }
    }

    return (
      <div style={root}>
        <Card style={card}>
          <div className='visit_date'>
            <DatePicker onChange={this.changeDate} style={{float: 'right'}} maxDate={new Date()} formatDate={(date) => moment(date).format('DD-MM-YYYY')} hintText={moment().format('DD-MM-YYYY')} mode='landscape' />
            <TextField onChange={this.handleSearch} style={{float: 'right', marginRight: '1em'}} hintText='Buscar visitante' />
          </div>
          <Table
            selectable={false}
            onCellClick={this.changeStatus}
          >
            <TableHeader
              displaySelectAll={false}
              adjustForCheckbox={false}
            >
              <TableRow>
                <TableHeaderColumn />
                <TableHeaderColumn className='visit_row'><label className='visit_header'>Visita</label></TableHeaderColumn>
                <TableHeaderColumn className='visit_row'><label className='visit_header'>Estado</label></TableHeaderColumn>
                <TableHeaderColumn className='visit_row'><label className='visit_header'>Ingreso</label></TableHeaderColumn>
                <TableHeaderColumn className='visit_row'><label className='visit_header'>Salida</label></TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody
              displayRowCheckbox={false}
            >
              {data.map((visit, index) => {
                let startdate = visit.start_date

                startdate = startdate.split(':')
                startdate = startdate[0] + ':' + startdate[1]

                startdate = moment.utc(startdate, 'YYYY-MM-DD"T"HH:mm').local()

                let exitdate = visit.exit_date
                if (exitdate !== null && (visit.status === 'finished' || visit.status === 'canceled')) {
                  exitdate = exitdate.split(':')
                  exitdate = exitdate[0] + ':' + exitdate[1]

                  exitdate = moment.utc(exitdate, 'YYYY-MM-DD"T"HH:mm').local().format('HH:mm')
                } else {
                  exitdate = null
                }

                let statusText = ''
                let status = ''
                let colorText = ''
                let colorIcon = ''

                if (visit.status === 'waiting' || visit.status === 'new') {
                  status = 'alarm'
                  colorText = '#FF9824'
                  colorIcon = '#FF9824'

                  if (visit.status === 'new') {
                    status = 'access_time'
                    colorText = '#FF4E55'
                    colorIcon = '#676a6c'
                  }

                  let minutes = moment().diff(startdate, 'minutes')
                  let hours = Math.floor((minutes / 60))
                  minutes %= 60
                  if (minutes < 10) {
                    minutes = '0' + minutes
                  }
                  if (hours < 10) {
                    hours = '0' + hours
                  }

                  statusText = 'Tiempo esperando ' + hours + ':' + minutes
                } else if (visit.status === 'accepted') {
                  status = 'directions_run'
                  colorText = '#00B114'
                  colorIcon = '#00B114'
                  statusText = 'Admitido'
                } else if (visit.status === 'finished') {
                  status = 'directions_run'
                  colorText = '#249BF0'
                  colorIcon = '#249BF0'
                  statusText = 'Terminada'
                } else if (visit.status === 'canceled') {
                  status = 'block'
                  colorText = '#FF4E55'
                  colorIcon = '#FF4E55'
                  statusText = 'Cancelada'
                }
                return (
                  <TableRow key={index}>
                    <TableRowColumn className='visit_row'><Avatar size={55} src={visit.visitor_photo} /></TableRowColumn>
                    <TableRowColumn className='visit_row'>
                      <label className='visit_name'>{visit.visitor_first_name} {visit.visitor_last_name}</label>
                      <br />
                      <label className='visit_establishment'>{visit.establishmentName} &#8226; {visit.affair}</label>
                    </TableRowColumn>
                    <TableRowColumn className='visit_row'>
                      <FontIcon className='material-icons' style={{color: colorIcon, fontSize: '2.5em'}}>{status}</FontIcon>
                      <br />
                      <label className='visit_status' style={{color: colorText}}>{statusText}</label>
                    </TableRowColumn>
                    <TableRowColumn className='visit_row'><label className='visit_time'>{startdate.format('HH:mm')}</label></TableRowColumn>
                    <TableRowColumn className='visit_row'><label className='visit_time'>{exitdate}</label></TableRowColumn>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
          <div className='visit_paginate'>
            {nextBtn}{backBtn}
          </div>
        </Card>
        <Visit visit={this.state.visit} onChange={this.onChangeStatus} />
      </div>
    )
  }
}

const mapStateProps = (store) => ({
  user: store.user
})

export default connect(mapStateProps, null)(Visits)
