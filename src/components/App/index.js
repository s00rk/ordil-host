import React, { Component } from 'react'
import AppBar from 'material-ui/AppBar'
import { connect } from 'react-redux'
import axios from 'axios'
import FontIcon from 'material-ui/FontIcon'
import constants from '../../constants.js'
import Error from '../../utils/errors.js'
import { login, logout, getUser } from '../../actions/userAction'

import Logged from '../Logged'
import Login from '../Login'
import Visits from '../Visits'

class App extends Component {
  componentDidMount () {
    const { user } = this.props
    if (user !== undefined && user.token) {
      axios.defaults.headers.common['Authorization'] = user.token
      this.props.getUserLoad()
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      isLogin: false,
      errorLogin: null
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleLogout = this.handleLogout.bind(this)
    this.onClickTitle = this.onClickTitle.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    this.setState({ isLogin: true, errorLogin: null })
    let token = null
    axios.post(constants.URL_LOGIN, { username: e.target.email.value, password: e.target.password.value, userType: 'host', mobileId: 'web' })
      .then((response) => {
        if ('error' in response.data) {
          this.setState({ errorLogin: Error(response.data.error), isLogin: false })
        } else {
          token = response.data.token
          axios.defaults.headers.common['Authorization'] = 'Token ' + token
          axios.get(constants.URL + 'hosts')
            .then((response) => {
              if (response.data.objects.length !== 1) {
                this.setState({ errorLogin: 'User not found', isLogin: false })
              } else {
                let user = response.data.objects[0]
                user['token'] = token
                this.setState({ errorLogin: null, isLogin: false })
                this.props.loginClick(user)
              }
            })
            .catch((err) => {
              console.log(err)
            })
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  handleLogout (e) {
    e.preventDefault()
    this.props.logoutClick()
    console.log('logout')
  }

  onClickTitle (e) {
    window.location = '/host/'
  }

  render () {
    const { user } = this.props
    if (user === undefined || !user.token) {
      return <Login onSubmit={this.handleSubmit} isLogin={this.state.isLogin} error={this.state.errorLogin} />
    }

    return (
      <div>
        <AppBar
          title='Ordil ID'
          iconElementRight={<Logged user={user} onLogout={this.handleLogout} />}
          iconElementLeft={<FontIcon className='material-icons' style={{color: 'white'}}>home</FontIcon>}
          onLeftIconButtonTouchTap={this.onClickTitle}
          iconStyleLeft={{display: 'flex', alignItems: 'center', marginTop: '0px', marginLeft: '0px', cursor: 'pointer'}}
        />
        <br />
        <Visits />
      </div>
    )
  }
}

const mapStateProps = (store) => ({
  user: store.user
})

const mapDispatchProps = (dispatch) => ({
  loginClick: (user) => {
    dispatch(login(user))
  },
  logoutClick: () => {
    dispatch(logout())
  },
  getUserLoad: () => {
    dispatch(getUser())
  }
})

export default connect(mapStateProps, mapDispatchProps)(App)
