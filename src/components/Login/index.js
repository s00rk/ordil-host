import React, { PureComponent } from 'react'
import { Card, CardText } from 'material-ui/Card'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import LinearProgress from 'material-ui/LinearProgress'

import './index.css'
import logo from './ordil.svg'

const styles = {
  margin: 'auto'
}

class Login extends PureComponent {
  render () {
    const { onSubmit, isLogin, error } = this.props

    let login = ''
    if (isLogin) {
      login = (
        <div>
          <br /><br />
          <LinearProgress mode='indeterminate' />
        </div>
      )
    }

    return (
      <div>
        <img className='login_logo' src={logo} width='135' alt='logo' />
        <div className='login_background' />
        <div className='login'>
          <div style={styles}>
            <Card>
              <CardText className='login_form'>
                <h1 className='login_title'>Bienvenido a Ordil Web</h1>
                <div style={{display: 'inline-flex', alignItems: 'center'}}>
                  <i className='fa fa-laptop' aria-hidden='true' style={{fontSize: '8em', marginRight: '0.3em'}} />
                  <form onSubmit={onSubmit}>
                    <TextField floatingLabelText='Email' type='email' name='email' errorText={error} fullWidth required />
                    <br />
                    <TextField floatingLabelText='Contraseña' type='password' name='password' fullWidth required />
                    <br />
                    <FlatButton href='#' label='Olvide mi contraseña' secondary /> <RaisedButton type='submit' label='Iniciar sesión' primary />
                    {login}
                  </form>
                </div>
              </CardText>
            </Card>
          </div>
        </div>
      </div>
    )
  }
}

export default Login
