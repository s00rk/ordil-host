import React, { Component } from 'react'
import { connect } from 'react-redux'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import Avatar from 'material-ui/Avatar'
import axios from 'axios'

import constants from '../../constants.js'

class Visit extends Component {
  componentDidMount () {
    axios.defaults.headers.common['Authorization'] = 'Token ' + this.props.user.token
  }

  constructor (props) {
    super(props)

    this.handleUpdate = this.handleUpdate.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  handleUpdate (e) {
    let action = e.target.innerText.toLowerCase()
    let status = null
    if (action === 'rechazar') {
      status = 'canceled'
    } else if (action === 'esperar') {
      status = 'waiting'
    } else if (action === 'admitir') {
      status = 'accepted'
    } else if (action === 'permitir salir') {
      status = 'finished'
    }

    if (status === null) {
      this.props.onChange(this.props.visit)
    }
    const { visit } = this.props
    axios.put(constants.URL + 'visits/' + visit.id, { status: status })
      .then((response) => {
        let data = response.data
        visit.status = data.status
        visit.attention_date = data.attention_date
        visit.exit_date = data.exit_date
        this.props.onChange(visit)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  handleClose () {
    this.props.onChange(this.props.visit)
  }

  render () {
    const { visit } = this.props
    if (visit === null) {
      return <div />
    }

    let actions = [
      <FlatButton
        label='Cerrar'
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label='Rechazar'
        style={{color: '#FF4E55'}}
        onTouchTap={this.handleUpdate}
      />,
      <FlatButton
        label='Esperar'
        style={{color: '#FF9824'}}
        onTouchTap={this.handleUpdate}
      />,
      <FlatButton
        label='Admitir'
        style={{color: '#249BF0'}}
        onTouchTap={this.handleUpdate}
      />
    ]

    if (visit.status === 'waiting') {
      actions.splice(2, 1)
    } else if (visit.status === 'accepted') {
      actions = [
        <FlatButton
          label='Cerrar'
          onTouchTap={this.handleClose}
        />,
        <FlatButton
          label='Permitir salir'
          style={{color: '#249BF0'}}
          onTouchTap={this.handleUpdate}
        />
      ]
    } else if (visit.status !== 'new') {
      actions = [
        <FlatButton
          label='Cerrar'
          onTouchTap={this.handleClose}
        />
      ]
    }

    return (
      <Dialog
        actions={actions}
        modal={false}
        open={(visit !== null)}
        onRequestClose={this.handleClose}
        >
        <div style={{display: 'inline-flex', alignItems: 'center'}}>
          <Avatar size={100} src={visit.visitor_photo} />
          <div style={{marginLeft: '2em'}}>
            <label className='visit_name'>{visit.visitor_first_name} {visit.visitor_last_name}</label>
            <br />
            <label className='visit_establishment'>{visit.affair} &#8226; {visit.establishmentName}</label>
          </div>
        </div>
      </Dialog>
    )
  }
}

const mapStateProps = (store) => ({
  user: store.user
})

export default connect(mapStateProps, null)(Visit)
