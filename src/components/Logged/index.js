import React, { Component } from 'react'
import Avatar from 'material-ui/Avatar'
import MenuItem from 'material-ui/MenuItem'
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'

const style = {
  display: 'flex',
  alignItems: 'center'
}

class Logged extends Component {
  constructor (props) {
    super(props)

    this.state = {
      openMenu: false
    }

    this.handleMenu = this.handleMenu.bind(this)
    this.handleOnRequestChange = this.handleOnRequestChange.bind(this)
  }

  handleMenu (e) {
    e.preventDefault()
    this.setState({ openMenu: true })
  }

  handleOnRequestChange (value) {
    this.setState({ openMenu: value })
  }

  render () {
    const { user, onLogout } = this.props
    return (
      <div style={style}>
        <label style={{marginRight: '1em', cursor: 'pointer', color: 'white'}} onClick={this.handleMenu}>{user.first_name} {user.last_name}</label>
        <Avatar style={{cursor: 'pointer'}} src={user.photo} onClick={this.handleMenu} />
        <IconMenu
          iconButtonElement={<IconButton />}
          open={this.state.openMenu}
          onRequestChange={this.handleOnRequestChange}
        >
          <MenuItem primaryText='Mi perfil' />
          <MenuItem onClick={onLogout} primaryText='Cerrar sesión' />
        </IconMenu>
      </div>
    )
  }
}

export default Logged
