const errors = {
  '900': 'Missing parameters',
  '901': 'Email/Password incorrect',
  '902': 'Email already taken',
  '903': 'You can not do this action',
  '904': 'Cant generate code invitation',
  '905': 'Code invalid',
  '906': 'Invitation already was sent',
  '907': 'This host is already connected',
  '908': 'Affair not found',
  '909': 'Host not found',
  '910': 'Visitor not found',
  '911': 'Invalid parameters',
  '912': 'Invalid user type for user',
  '913': 'Department not found',
  '914': 'Email/Code invalid',
  '915': 'Relation host establishment does not exists',
  '916': 'Establishment does not exists'
}

export default (code) => errors[code]
