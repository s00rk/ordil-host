import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import axios from 'axios'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

axios.defaults.headers.common['Content-Type'] = 'application/json'

import App from './components/App'
import './index.css'

import store from './store'

import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()

if ('Notification' in window) {
  window.Notification.requestPermission()
}

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider>
      <App />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
)
